﻿# Joeghanoe Bhatti - Testen

Aan de hand van de requierements zal ik testen of de gebouwde 

<div style="padding-bottom: 24px;">
<h3>Test 1 - Is de basis premie calculatie correct</h3>
<h4>Doel</h4>
<p>Kijken of de berekening die gebruikt wordt voor het uitrekenen van de basispremie juist verloopt. De berekening is: <code>(voertuig / 100 - leeftijd + vermogen in KW / 5) / 3</code>.</p>
<h4>Technieken</h4>
<p>Aangezien deze berekening maar een uitkomst kan hebben wil ik gebruik maken van een Fact.</p>
<h4>Dataset</h4>
<p>Aangezien ik een Fact gebruik is mijn dataset vast en heb ik maar een verwacht uitkomen.</p>
<hr>
<h4>Geteste Factor:</h4>
<p>Is het uitkomen van de gegeven formule gelijk aan het verwachte uitkomen.</p>
<h4>Verwachting:</h4>
<p>Dat de uitkomst van de som gelijk is aan de verwachtte uitkomst. Aangezien de som statisch is en geen dynamische facoteren heeft die het kunnen beinvloeden is dit de meest logische manier van denken.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na de test gelopen te hebben heb ik ondervonden dat de berekening die uitgevoerd wordt onjuist is. De som was namelijk: <code>voertuig / 100 - leeftijd + vermogen in KW / 5 / 3</code> en zoals we op de basisschool geleerd hebben gaat delen voor plus en min dus moesten er haken om de berekening tot voor de / 3. Na het aangepast te hebben was de som gelijk aan de verwachtte uitkomst. Aangezien de som statisch is en geen dynamische facoteren heeft die het kunnen beinvloeden is dit de meest logische manier van denken.</p>
</div>

<div style="padding-bottom: 24px;">
<h3>Test 2 - 15% Premie opslag</h3>
<h4>Doel</h4>
<p>Bekijken of de gebruiker een 15% opslag van premie krijgt als hij/zij:</p>
<ol>
<li>Jonger is dan 23 jaar oud</li>
<li>Korter dan 5 jaar zijn rijbewijs in bezit heeft</li>
</ol>
<h4>Technieken</h4>
<p>Voor deze test gebruik ik een theory waar ik de grenswaarden af ga van de functie die aangeroepen wordt voor het checken van de premie.</p>
<h4>Dataset</h4>
<table>
    <tr>
        <th>Leeftijd</th>
        <th>Startdatum Rijbewijs</th>
        <th>Verwachtte uitkomen</th>
    </tr>
    <tr>
        <td>22</td>
        <td>26-04-2000</td>
        <td>12.65</td>
    </tr>
    <tr>
        <td>23</td>
        <td>26-04-2000</td>
        <td>11</td>
    </tr>
    <tr>
        <td>23</td>
        <td>26-04-2016</td>
        <td>11</td>
    </tr>
    <tr>
        <td>23</td>
        <td>26-04-2017</td>
        <td>12.65</td>
    </tr>
    <tr>
        <td>30</td>
        <td>26-04-2016</td>
        <td>11</td>
    </tr>
    <tr>
        <td>30</td>
        <td>26-04-2017</td>
        <td>12.65</td>
    </tr>
</table>
<hr>
<h4>Geteste Factor:</h4>
<p>Krijgt de gebruiker een premie opslag van 15% wanneer hij/zij jonger is dan 23 jaar of korter dan vijf jaar zijn rijbewijs heeft aan de hand van de <code>PolicyHolder</code> klasse</p>
<h4>Verwachting:</h4>
<p>Ik verwacht dat mensen die voor 2016 hun rijbewijs hebben gehaald en ouder zijn dan 23 jaar geen premie opslag zullen krijgen. Alle mensen die niet aan deze criteria voldoen zullen volgens mij een opslag krijgen.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na mijn test te hebben gelopen met de bovenstaande dataset heb ik ondervonden dat er in de code werd gechekt op <code>policyHolder.LicenseAge <= 5</code> dit is niet correct aangezien de policyHolder korter dan vijf jaar zijn rijbewijs moest hebben en niet vijf jaar of korter.dit heb ik in mijn eindversie dus aangepast zodat de test volledig is.</p>
</div>

<div style="padding-bottom: 24px;">
<h3>Test 3 - Postcode premies</h3>
<h4>Doel</h4>
<p>Onderzoeken of de premies juist berekend worden aan de hand van de postcodes als volgt</p>
<ul>
<li>Postcodes 10xx - 35xx: 5% risico opslag</li>
<li>Postcodes 36xx - 44xx: 2% risico opslag</li>
</ul>
<h4>Technieken</h4>
<p>Voor deze test gebruik ik een theory waar ik de equivilentieklassen af ga en doormiddel van het aanroepen van de functie kan kijken of bij de juiste postcode de juiste premie meegegeven wordt.</p>
<h4>Dataset</h4>
<table>
    <tr>
        <th>Postcode</th>
        <th>Verwachtte uitkomen</th>
    </tr>
    <tr>
        <td>1000</td>
        <td>13.28</td>
    </tr>
    <tr>
        <td>3599</td>
        <td>13.28</td>
    </tr>
    <tr>
        <td>3600</td>
        <td>12.9</td>
    </tr>
    <tr>
        <td>4499</td>
        <td>12.9</td>
    </tr>
    <tr>
        <td>4500</td>
        <td>12.65</td>
    </tr>
    <tr>
        <td>30</td>
        <td>26-04-2017</td>
    </tr>
</table>
<hr>
<h4>Geteste Factor:</h4>
<p>Is de premie 5% hoger voor postcodes vanaf 1000 tot aan 3599 en 2% hoger voor postcodes van 3600 tot 4499 en wordt vanaf 4500 en hoger de premie weer normaal?</p>
<h4>Verwachting:</h4>
<p>Ik verwacht dat de premie aan de hand van de data juist uitgerekend wordt. Wel verwacht ik enige problemen met afrondingen aangezien dat ook een requierement is op de lijst. Deze is de volgende test die ik ga omschrijven.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na mijn test heb ik kunnen ondervinden dat in deze test in principe niets fout gaat. Alle postcodes die binnen de klassen vallen krijgen de juiste premies.</p>
</div>

<div style="padding-bottom: 24px;">
<h3>Test 4 - Afronding</h3>
<h4>Doel</h4>
<p>Testen of de premie aan het eind van de berekening juist wordt afgerond naar twee decimalen</p>
<h4>Technieken</h4>
<p>Voor deze test zal ik een Fact gebruiken aangezien het gaat om het bewijzen dat het demicaal wordt toegevoegd aan de waarde.</p>
<h4>Dataset</h4>
<p>Voor deze berekening heb ik als dataset gekozen voor het afgeronde nummer uit de eindberekening van de premie na het gebruiken van de consoleApp zelf. Dit was <code>12.65</code></p>
<hr>
<h4>Geteste Factor:</h4>
<p>Testen of het eindgetal van de <code>PremiumCalculation</code> overeenkomt met het getal dat uit de consoleApp komt.</p>
<h4>Verwachting:</h4>
<p>Ik verwacht dat het getal onjuist afgerond wordt in de calculator zelf. Na het lezen van de code ben ik erachter gekomen dat de afronding gebeurt in de program.cs file en niet in de premiumCalculator zelf.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na het testen heb ik inderdaad kunnen ondervinden dat het getal nog niet werd afgerond en dat heb ik nu aangepast zodat de test correct wordt uitgevoerd.</p>
</div>

<div style="padding-bottom: 24px;">
<h3>Test 5 - WA Plus is 20% duurder</h3>
<h4>Doel</h4>
<p>Constateren dat WA plus met het zelfde voertuig en policyholder 20% duurder is.</p>
<h4>Technieken</h4>
<p>Ik ga gebruik maken van een Fact aangezien ik hier één ding moet bewijzen.</p>
<h4>Dataset</h4>
<p>Ik maak gebruik van het zelfde vehicle en policyholder. Vervolgens maak ik twee PremiumCalculations aan één met de WA en één met de WA Plus verzekering om ze vervolgens te vergelijken op de 20% verschil.</p>
<hr>
<h4>Geteste Factor:</h4>
<p>Is de WA Plus verzekering 20% duurder dan de normale WA verzekering?</p>
<h4>Verwachting:</h4>
<p>Ik verwacht dat de berekening juist gaat aangezien het een statische berekening is en alle vergelijkingen al in de code omschreven staan.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na het testen heb ik ondervonden dat de berekening juist verloopt.</p>
</div>

<div style="padding-bottom: 24px;">
<h3>Test 6 - All Risk is twee keer zo duur</h3>
<h4>Doel</h4>
<p>Constateren dat All Risk met het zelfde voertuig en policyholder 2 keer zo duur is.</p>
<h4>Technieken</h4>
<p>Ik ga gebruik maken van een Fact aangezien ik hier één ding moet bewijzen.</p>
<h4>Dataset</h4>
<p>Ik maak gebruik van het zelfde vehicle en policyholder. Vervolgens maak ik twee PremiumCalculations aan één met de WA en één met de WA Plus verzekering om ze vervolgens te vergelijken op de 100% verschil tussen de twee.</p>
<hr>
<h4>Geteste Factor:</h4>
<p>Is de All Risk verzekering 100% duurder dan de normale WA verzekering?</p>
<h4>Verwachting:</h4>
<p>Ik verwacht dat de berekening juist gaat aangezien het een statische berekening is en alle vergelijkingen al in de code omschreven staan.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na het testen heb ik ondervonden dat de berekening juist verloopt.</p>
</div>

<div style="padding-bottom: 24px;">
<h3>Test 7 - Meer dan 6 Schadevrije jaren voor korting</h3>
<h4>Doel</h4>
<p>Ondervinden of je na 6 schadevrije jaren opeen tellend 5 procent korting tot aan de 65% krijgt.</p>
<h4>Technieken</h4>
<p>Ik maak gebruik van een theory om meerdere waardes in een keer te kunnen testen. Ik test hierbij alle equivelentieklassen.</p>
<h4>Dataset</h4>
<table>
    <tr>
        <th>Jaren</th>
        <th>Verwachtte uitkomen</th>
    </tr>
    <tr>
        <td>5</td>
        <td>12.65</td>
    </tr>
    <tr>
        <td>6</td>
        <td>12.02</td>
    </tr>
    <tr>
        <td>17</td>
        <td>5.06</td>
    </tr>
    <tr>
        <td>18</td>
        <td>4.43</td>
    </tr>
    <tr>
        <td>19</td>
        <td>4.43</td>
    </tr>
</table>
<hr>
<h4>Geteste Factor:</h4>
<p>Krijg ik meer korting nadat ik vijf jaar schadevrij heb gereden, zo ja is deze korting dan tot de 65%?</p>
<h4>Verwachting:</h4>
<p>Ik verwacht dat de berekening hier juist zal verlopen aangezien dit net als de voorgaande testen een makkelijke som is.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na het testen en wat debuggen heb ik ondervonden dat er een int gebruikt werd op een plek waar een double moest staan, hierdoor kwamen de berekeningen initieel niet uit als verwacht. Na het aanpassen van de int naar een double lopen alle berekeningen zoals verwacht.</p>
</div>

<div style="padding-bottom: 24px;">
<h3>Test 8 - Jaar Abbonement geeft 2.5% korting</h3>
<h4>Doel</h4>
<p>Onderzoeken of je bij een jaar abbonement 2.5% korting krijgt op je premie in tegenstelling tot de maandabbonementen.</p>
<h4>Technieken</h4>
<p>Gebruik maken van een Fact om een harde berekening uit te voeren en het jaar met de maand te vergelijken</p>
<h4>Dataset</h4>
<p>Ik ga gebruik maken van een statische dataset, ik doe de berekening <code>(Jaar / 1.025) / 12</code></p>
<hr>
<h4>Geteste Factor:</h4>
<p>Is het jaarabbonement 2.5% goedkoper dan het maandabbonement op jaarbasis?</p>
<h4>Verwachting:</h4>
<p>Ik denk dat het jaarabbonement 2.5% goedkoper gaat zijn met de code die nu staat. Ik heb er nog geen problemen mee kunnen vinden.</p>
<hr>
<h4>Ondervinding:</h4>
<p>Na het testen en de som juist krijgen heb ik kunnen ondervinden dat het jaarabbonement inderdaad 2.5% goedkoper is dan het maandabbonement op jaarbasis.</p>
</div>

## Overige Testen
* In policyholder heb ik validatie voor de datum toegevoegd
* In policyholder heb ik validatie dat de leeftijd over 18 moet zijn toegevoegd
* In policyholder heb ik validatie voor de postcode toegevoegd
* In policyholder heb ik de age property aangepast zodat hij een MinValue van 0 heeft.
* In vehicle heb ik de Age property aangepast zodat hij een MinValue van 0 heeft.