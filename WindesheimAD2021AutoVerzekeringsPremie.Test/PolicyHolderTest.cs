﻿using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie.Test
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData(18, 18)]
        public void IsAgeOverEightteen(int age, int ExpectedAge)
        {
            PolicyHolder policyHolder = new PolicyHolder(age, "26-04-2000", 1349, 0);

            int ActualAge = policyHolder.Age;

            // Check if age can be a negative number
            Assert.Equal(ExpectedAge, ActualAge);
        }

        [Theory]
        [InlineData(1333, 1333)]
        public void IsPostalCodeFourNumbers(int postalCode, int ExpectedPostalCode)
        {
            PolicyHolder policyHolder = new PolicyHolder(21, "26-04-2000", postalCode, 0);

            int ActualPostalCode = policyHolder.PostalCode;

            // Check if age can be a negative number
            Assert.Equal(ExpectedPostalCode, ActualPostalCode);
        }

        [Theory]
        [InlineData("26-04-2000", 21)]
        [InlineData("26-04-2021", 0)]
        [InlineData("26-04-2022", 0)]
        public void IsDriverLicenseStartDateAnActualDate(string DriverLicenseStartDate, int ExpectedDriverLicenseStartDate)
        {
            PolicyHolder policyHolder = new PolicyHolder(21, DriverLicenseStartDate, 1234, 0);

            int ActualDriverLicenseStartDate = policyHolder.LicenseAge;

            // Check if age can be a negative number
            Assert.Equal(ExpectedDriverLicenseStartDate, ActualDriverLicenseStartDate);
        }

    }
}
