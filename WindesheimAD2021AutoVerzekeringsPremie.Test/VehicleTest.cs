﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie.Test
{
    public class VehicleTest
    {
        [Theory]
        [InlineData(2020, 1)]
        [InlineData(2021, 0)]
        [InlineData(2022, 0)]
        public void IsVehicleAgeSetCorrectly(int ConstructionYear, int ExpectedOutcome)
        {
            Vehicle vehicle = new Vehicle(235, 3000, ConstructionYear);

            int Age = vehicle.Age;

            Assert.Equal(ExpectedOutcome, Age);
        }
    }
}
