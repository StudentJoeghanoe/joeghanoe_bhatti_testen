using System;
using System.Diagnostics;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;

namespace WindesheimAD2021AutoVerzekeringsPremie.Test
{
    public class PremiumCalculationTest
    {
        [Fact]
        public void isCalculateBasePremiumCorrect()
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(227, 3000, 1987);

            // Calculate a base premium based on this premium
            double ActualPremiumCalculation = CalculateBasePremium(testVehicle);

            // Set expectedOutcome for Fact
            double expectedOutcome = 13;
           
            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(expectedOutcome, ActualPremiumCalculation);
        }

        [Theory]
        [InlineData(22, 21, 14.95)]
        [InlineData(23, 21, 13)]
        [InlineData(23, 5, 13)]
        [InlineData(23, 4, 14.95)]
        [InlineData(30, 5, 13)]
        [InlineData(30, 4, 14.95)]
        public void DoesPremiumApplyFifteenPercentMarkupWhenPersonHasObtainedDrivingLicenseLessThanFiveYearsAgoOrHasBeenBornLessThanTwentyThreeYearsAgo(int Age, int DriversLicenseStartDate, double ExpectedOutcome)
        {
            // Create a vehicle
            var MockVehicle = new Mock<IVehicle>();
            MockVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(3000);
            MockVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(227);
            MockVehicle.Setup(vehicle => vehicle.Age).Returns(34);

            // Create a PolicyHolder
            var MockPolicyHolder = new Mock<IPolicyHolder>();
            MockPolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(Age);
            MockPolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(DriversLicenseStartDate);
            MockPolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(0);
            MockPolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(5000);

            // Create a PremiumCalculation from vehicle, policyholder and base coverage
            PremiumCalculation premiumCalculation = new(MockVehicle.Object, MockPolicyHolder.Object, InsuranceCoverage.WA);

            // Get separate variable for the comparison
            double actualPremiumCalculation = premiumCalculation.PremiumAmountPerYear;

            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(ExpectedOutcome, actualPremiumCalculation);
        }

        [Theory]
        [InlineData(1000, 15.7)]
        [InlineData(3599, 15.7)]
        [InlineData(3600, 15.25)]
        [InlineData(4499, 15.25)]
        [InlineData(4500, 14.95)]
         public void DoesPostalCodePremiumGetApplied(int PostalCode, double ExpectedOutcome)
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(227, 3000, 1987);

            // Create a PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(22, "26-04-2016", PostalCode, 0);

            // Create a PremiumCalculation from vehicle, policyholder and base coverage
            PremiumCalculation actualPremiumCalculation = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(ExpectedOutcome, actualPremiumCalculation.PremiumAmountPerYear);
        }

        [Fact]
        public void WaPlusIsTwentyPercentMoreExpensive()
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(227, 3000, 1987);

            // Create a PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(22, "26-04-2016", 5000, 0);

            // Setup Premiumcalculation for base WA
            PremiumCalculation premiumCalculationWA = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Setup Premiumcalculation for base WA
            PremiumCalculation premiumCalculationWAPlus = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.WA_PLUS);

            // Get separate variable for the comparison
            double ActualDividedPremiumByTwentyPercent = (premiumCalculationWAPlus.PremiumAmountPerYear / 120) * 100;

            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(premiumCalculationWA.PremiumAmountPerYear, ActualDividedPremiumByTwentyPercent, 2);
        }

        [Fact]
        public void AllRiskIsTwiceAsExpensive()
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(227, 3000, 1987);

            // Create a PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(22, "26-04-2016", 5000, 0);

            // Setup Premiumcalculation for base WA
            PremiumCalculation premiumCalculationWA = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Setup Premiumcalculation for base WA
            PremiumCalculation premiumCalculationWAPlus = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.ALL_RISK);

            // Get separate variable for the comparison
            double ActualDividedPremiumByHalf = (premiumCalculationWAPlus.PremiumAmountPerYear / 200) * 100;

            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(premiumCalculationWA.PremiumAmountPerYear, ActualDividedPremiumByHalf);
        }

        [Theory]
        [InlineData(5, 14.95)]
        [InlineData(6, 14.2)]
        [InlineData(17, 5.98)]
        [InlineData(18, 5.23)]
        [InlineData(19, 5.23)]

        public void NoClaimForMoreThanFiveYearsGivesAFivePercentDiscount(int Years, double ExpectedOutcome)
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(227, 3000, 1987);

            // Create a PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(22, "26-04-2016", 5000, Years);

            // Create a PremiumCalculation from vehicle, policyholder and base coverage
            PremiumCalculation ActualOutcome = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(ExpectedOutcome, ActualOutcome.PremiumAmountPerYear);
        }

        [Fact]
        public void YearSubscriptionGivesTwoAndAHalfPercentDiscount()
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(227, 3000, 1987);

            // Create a PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(22, "26-04-2016", 5000, 0);

            // Create a PremiumCalculation from vehicle, policyholder and base coverage
            PremiumCalculation calculation = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Get month and year premium payment amount
            double YearPremium = calculation.PremiumPaymentAmount(PaymentPeriod.YEAR);
            double MonthPremium = calculation.PremiumPaymentAmount(PaymentPeriod.MONTH);

            // Make month premium of the year premium amount
            double YearMonthPremium = (YearPremium * 1.025) / 12;

            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(MonthPremium, YearMonthPremium, 2);
        }

        [Fact]
        public void PremiumGetsRoundedToTwoDecimals()
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(227, 3000, 1987);

            // Create a PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(22, "26-04-2016", 5000, 0);

            // Create a PremiumCalculation from vehicle, policyholder and base coverage
            PremiumCalculation ActualPremium = new PremiumCalculation(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Set expectedOutcome for Fact
            double expectedOutcome = 14.95;

            // Compare the expectedOutcome to the actualOutcome
            Assert.Equal(expectedOutcome, ActualPremium.PremiumAmountPerYear);
        }
    }
}
