﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    class PolicyHolder : IPolicyHolder
    {
        public int Age { get; private set; }
        public int LicenseAge { get; private set; }
        public int PostalCode { get; private set; }
        public int NoClaimYears { get; private set; }

        internal PolicyHolder(int Age, string DriverlicenseStartDate, int PostalCode, int NoClaimYears )
        {
            // Check if Age is over eighteen
            if (Age >= 18)
            {
                this.Age = Age;
            }
            else
            {
                throw new ArgumentException(nameof(Age));
            }

            // Check if zipcode is valid
            if (PostalCode is >= 1000 and < 9999)
            {
                this.PostalCode = PostalCode;
            }
            else
            {
                throw new ArgumentException(nameof(PostalCode));
            }

            this.NoClaimYears = NoClaimYears;

            // Check if format is actual date
            LicenseAge = AgeByDate(ParseDate(DriverlicenseStartDate));
        }

        private static DateTime ParseDate(string dateStr)
        {
            var cultureInfo = new CultureInfo("nl-NL");
            DateTime value;

            if (DateTime.TryParse(dateStr, cultureInfo, DateTimeStyles.None, out value))
            {
                return value;
            }
            else
            {
                throw new ArgumentException(nameof(dateStr));
            }
        }

        private static int AgeByDate(DateTime date)
        {
            var today = DateTime.Today;
            var age = Math.Max(0, today.Year - date.Year);

            return age;
        }

    }
}
