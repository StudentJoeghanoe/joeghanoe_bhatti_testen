﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    interface IVehicle
    {
        public int PowerInKw { get; }
        public int ValueInEuros { get; }
        public int Age { get; }
    }
}
