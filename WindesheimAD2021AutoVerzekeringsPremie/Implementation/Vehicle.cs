﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    class Vehicle : IVehicle
    {
        public int PowerInKw { get; private set; }
        public int ValueInEuros { get; private set; }

        public int Age { get; private set; }

        internal Vehicle (int PowerInKw, int ValueInEuros, int constructionYear)
        {
            this.PowerInKw = PowerInKw;
            this.ValueInEuros = ValueInEuros;
            Age = Math.Max(0, DateTime.Now.Year - constructionYear);            
        }
    }
}
