﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    internal class PremiumCalculation
    {
        public double PremiumAmountPerYear { get; private set; }
        private readonly int PRECISION = 2;

        internal enum PaymentPeriod
        {
            YEAR,
            MONTH
        }
                
        internal PremiumCalculation(IVehicle vehicle, IPolicyHolder policyHolder, InsuranceCoverage coverage)
        {
            
            double premium = 0d;
            premium += CalculateBasePremium(vehicle);

            if(policyHolder.Age < 23 || policyHolder.LicenseAge < 5)
            {
                premium *= 1.15;
            }

            premium = UpdatePremiumForPostalCode(premium, policyHolder.PostalCode);

            if(coverage == InsuranceCoverage.WA_PLUS)
            {
                premium *= 1.2;
            } else if (coverage == InsuranceCoverage.ALL_RISK)
            {
                premium *= 2;
            }

            premium = UpdatePremiumForNoClaimYears(premium, policyHolder.NoClaimYears);
            
            // Added rounding as per requierements
            PremiumAmountPerYear = Math.Round(premium, PRECISION);
        }

        private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            // Changed to double as the calculation would not perform with an int
            double NoClaimPercentage = (years - 5) * 5;
            if (years >= 19) { NoClaimPercentage = 65; }
            if (years < 6) { NoClaimPercentage = 0; }
            return premium * ((100 - NoClaimPercentage) / 100);
        }

        private static double UpdatePremiumForPostalCode(double premium, int postalCode) => postalCode switch
        {
            >= 1000 and < 3600 => premium * 1.05,
            < 4500 => premium * 1.02,
            _ => premium,
        };

        internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear / 1.025 : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }

        internal static double CalculateBasePremium(IVehicle vehicle)
        {
            // (Waarde voertuig / 100 - leeftijd + vermogen in KW / 5) / 3
            return (vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5) / 3;
        }
    }
}
