﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

// Added internalsvisibileto to make the functions appear here
[assembly: InternalsVisibleTo("WindesheimAD2021AutoVerzekeringsPremie.Test")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
}
